# mini_robot_control

Mini app for harvesting robots

If flutter is already installed just use `flutter pub get` to download dependencies then skip to [build](#build)

## System setup
Some informations to install flutter and other dependencies

### Flutter
put sources in usr/local : `cd /usr/local/lib`  
clone repo: `git clone https://github.com/flutter/flutter.git -b stable`  
create links for binaries :  
`sudo ln -s /usr/local/lib/flutter/bin/flutter /usr/local/bin/flutter`  
`sudo ln -s /usr/local/lib/flutter/bin/dart /usr/local/bin/dart`

you can use `flutter doctor` to check if something is missing

### Android Studio

Download: https://developer.android.com/studio/  
Ubuntu : `sudo apt install libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1 libbz2-1.0:i386`

Install studio :  
`cd /opt`  
`tar xf ~/Downloads/android-studio-2021.1.1.21-linux.tar.gz`  
`sudo ln -s /opt/android-studio/bin/studio.sh /usr/local/bin/studio`

Desktop integration :  
launch studio : `studio`  
click on configuration (resize th window if necessary)  
Select "create desktop entry"

command line tools dependency :  
Tools > SDK Manager > SDK Tools > Android SDK Command-line Tools (latest)

### Chrome
install chrome or chromium :  
chrome : https://www.google.com/chrome/  
chromium : `sudo apt install chromium-browser`

setup env : `echo export CHROME_EXECUTABLE="$(which <chrome|chromium>)" >> .profile`

### gPRC

#### Compiler

(download protoc compiler)[https://github.com/protocolbuffers/protobuf/releases]

unzip it somewhere (like /opt/protoc)

add protoc to path

```
$ echo "export PATH=$PATH:<path_to_protoc_root>/bin" >> .bashrc
$ source .bashrc
```

activate gRPC : `flutter pub global activate protoc_plugin`

add pluggins to path : `export PATH="$PATH":"$HOME/.pub-cache/bin"`

## Directory structure
layout => main layout components like navigation menu
models => data models
pages => app pages 
proto => proto buffers
sections => section components of the pages
services => all app services
styles => styles and theme files
widgets => small widgets and small standard components

## Build
build an apk: `flutter build apk --split-per-abi`  
this will create an apk for each abi in `<project>/build/app/outputs/flutter-apk/`

build web app: `flutter build web`  
app files are put in `<project>/build/web/` and can be tested with a simple http server like python http.server: `python3 -m http.server 8000`

a makefile summarize this:
- make apk: build apk
- make web: build web app
- make serve_http: launch http server for web app at localhost:8000

## App use

The app has only two pages with a simple menu to navigate

### Home page
This page display visual data.
All data are mock in mocks/data_mock.dart.

It is possible to add (put not remove) a harvest to the data table and the chart will update with the changes,
click "Ajouter" and fill the form
Acceptable date format are formats parsable by DateTime of Dart, [list here](https://api.dart.dev/stable/2.16.1/dart-core/DateTime/parse.html)
Weight must be a valid number
None can be empty

