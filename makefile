deps:
	flutter pub get

apk: deps
	flutter build apk --split-per-abi

web: deps
	flutter build web

serve_http: web
	python3 -m http.server 8000 --directory build/web