
import 'dart:math';

import 'package:mini_robot_control/models/harvest_details.dart';

import '../proto/generated/robot.pb.dart';

class DataMock {
  static final harvests = [
    Harvest(date: DateTime.now().subtract(const Duration(days: 7)).toString(), weight: 1000.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 6)).toString(), weight: 1200.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 5)).toString(), weight: 900.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 4)).toString(), weight: 2000.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 3)).toString(), weight: 1300.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 2)).toString(), weight: 1800.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 1)).toString(), weight: 2200.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 7)).toString(), weight: 1100.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 6)).toString(), weight: 900.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 5)).toString(), weight: 800.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 4)).toString(), weight: 500.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 3)).toString(), weight: 900.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 2)).toString(), weight: 1500.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 1)).toString(), weight: 1600.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 7)).toString(), weight: 1300.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 6)).toString(), weight: 1800.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 5)).toString(), weight: 2000.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 4)).toString(), weight: 1900.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 3)).toString(), weight: 2200.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 2)).toString(), weight: 2500.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 1)).toString(), weight: 3000.0, robot: "003"),
  ];
  static final robots = [
    Robot(id: "001", activated: 0),
    Robot(id: "002", activated: 1),
    Robot(id: "003", activated: 0),
  ];

  static const harvestMaxWeight = 3200.0;
  static const harvestMinWeight = 400.0;

  DataMock();
}