import 'dart:math';

import 'package:grpc/grpc.dart';
import 'package:mini_robot_control/proto/generated/robot.pbgrpc.dart';

class RobotManagerService extends RobotManagerServiceBase{
  final harvests = [
    Harvest(date: DateTime.now().subtract(const Duration(days: 7)).toString(), weight: 1000.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 6)).toString(), weight: 1200.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 5)).toString(), weight: 900.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 4)).toString(), weight: 2000.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 3)).toString(), weight: 1300.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 2)).toString(), weight: 1800.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 1)).toString(), weight: 2200.0, robot: "001"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 7)).toString(), weight: 1100.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 6)).toString(), weight: 900.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 5)).toString(), weight: 800.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 4)).toString(), weight: 500.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 3)).toString(), weight: 900.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 2)).toString(), weight: 1500.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 1)).toString(), weight: 1600.0, robot: "002"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 7)).toString(), weight: 1300.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 6)).toString(), weight: 1800.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 5)).toString(), weight: 2000.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 4)).toString(), weight: 1900.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 3)).toString(), weight: 2200.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 2)).toString(), weight: 2500.0, robot: "003"),
    Harvest(date: DateTime.now().subtract(const Duration(days: 1)).toString(), weight: 3000.0, robot: "003"),
  ];
  final robots = [
    Robot(id: "001", activated: 0),
    Robot(id: "002", activated: 1),
    Robot(id: "003", activated: 0),
  ];

  @override
  Future<HarvestListReply> getHarvests(ServiceCall call, HarvestListRequest request) async {
    var reply = HarvestListReply()..harvests.addAll(harvests);
    reply.max = 3200;
    reply.min = 400;
    return reply;
  }

  @override
  Future<RobotListReply> getRobots(ServiceCall call, RobotListRequest request) async {
    return RobotListReply()..robots.addAll(robots);
  }

}

Future<void> main(List<String> args) async {
  final server = Server(
    [RobotManagerService()],
    const <Interceptor>[],
    CodecRegistry(codecs: const [GzipCodec(), IdentityCodec()])
  );
  await server.serve(port: 50051);
  print('Server listening on port ${server.port}...');
}