class RobotDetails {
  final String id;
  final bool activated;
  RobotDetails(this.id, this.activated);
}