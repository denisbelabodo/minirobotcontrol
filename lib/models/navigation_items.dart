import 'package:flutter/material.dart';

enum NavigationItems {
  home,
  profile
}

extension NavigationItemsExtensions on NavigationItems {
  IconData get icon {
    switch (this) {
      case NavigationItems.home:
        return Icons.home;
      case NavigationItems.profile:
        return Icons.person;
      default:
        return Icons.person;
    }
  }
}