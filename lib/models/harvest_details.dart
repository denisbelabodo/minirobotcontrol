
class HarvestDetails {
  final String id;
  final DateTime date;
  final double weight;
  final Duration delay;

  HarvestDetails(this.id, this.date, this.weight, this.delay);
}

class HarvestData {
  final double max;
  final double min;
  final List<HarvestDetails> harvests;
  HarvestData(this.max, this.min, this.harvests);
}