import 'package:flutter/material.dart';
import 'package:mini_robot_control/pages/profile.dart';
import 'app_layout.dart';
import '../pages/home.dart';
import '../models/navigation_items.dart';
import 'responsive.dart';
import '../widgets/navigation_button.dart';


/**
 * Navigation widget
 */
class NavigationPanel extends StatefulWidget {
  final Axis axis;
  final int activeTab;

  const NavigationPanel({Key? key, required this.axis, required this.activeTab}) : super(key: key);

  @override
  _NavigationPanelState createState() => _NavigationPanelState();
}

class _NavigationPanelState extends State<NavigationPanel> {

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: const BoxConstraints(minWidth: 80),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      margin: Responsive.isDesktop(context)
          ? const EdgeInsets.symmetric(horizontal: 30, vertical: 20)
          : const EdgeInsets.all(10),
      child: widget.axis == Axis.vertical ? verticalBar() : horizontalBar(),
    );
  }
  
  Widget horizontalBar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: NavigationItems.values.map( (e) => NavigationButton(
        onPressed: () {
          setState(() {
            if (e.name == NavigationItems.profile.name) {
              Navigator.of(context).pushNamed("/user");
            } else if (e.name == NavigationItems.home.name) {
              Navigator.of(context).pushNamed("/home");
            }
          });
        },
        icon: e.icon,
        isActive: e.index == widget.activeTab,
      )
      ).toList(),
    );
  }

  Widget verticalBar() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: NavigationItems.values.map( (e) => NavigationButton(
                    onPressed: () {
                      setState(() {
                        if (e.name == NavigationItems.profile.name) {
                          Navigator.of(context).pushNamed("/user");
                        } else if (e.name == NavigationItems.home.name) {
                          Navigator.of(context).pushNamed("/home");
                        }
                      });
                    },
                    icon: e.icon,
                    isActive: e.index == widget.activeTab,
                  )
                ).toList(),
        );
  }
}

