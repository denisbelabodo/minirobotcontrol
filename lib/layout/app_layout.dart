import 'package:flutter/material.dart';
import 'package:mini_robot_control/layout/responsive.dart';

import 'navigation_panel.dart';

/**
 * Main layout, setup the navigation menu and responsive layout
 */
class AppLayout extends StatelessWidget {
  final Widget content;
  final int activeTab;

  const AppLayout({Key? key, required this.content, required this.activeTab}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Responsive(
      // Mobile Layout
        mobile: Column(
          children: [
            // const TopAppBar(),
            Expanded(child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: content,
            )),
             NavigationPanel(
              axis: Axis.horizontal,
              activeTab: activeTab
            )
          ],
        ),
        // Desktop Layout
        desktop: Row (
          children: [
            NavigationPanel(
                axis: Axis.vertical,
                activeTab: activeTab,
            ),
            Expanded(
              flex: 5,
              child: Padding(
                padding: const EdgeInsets.only(right: 10.0, bottom: 20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    // const SizedBox(height: 100, child: TopAppBar()),
                    Expanded(child: content)
                  ],
                ),
              ),
            )
          ],
        )
    );
  }
}