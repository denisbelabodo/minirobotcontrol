import 'package:flutter/material.dart';
import 'package:mini_robot_control/pages/home.dart';
import 'package:mini_robot_control/pages/profile.dart';
import 'package:mini_robot_control/styles/styles.dart';

import 'layout/app_layout.dart';

// Based on https://github.com/imariman/fintech_dashboard_clone?ref=flutterawesome.com

void main() {
  runApp(const MiniRobotControl());
}

class MiniRobotControl extends StatelessWidget {
  const MiniRobotControl({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mini robot control',
      theme: ThemeData(
        scaffoldBackgroundColor: Styles.scaffoldBackgroundColor,
        scrollbarTheme: Styles.scrollbarTheme,
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.black,
          foregroundColor: Colors.white,
        )
      ),
      home: HomePage(),
      routes: <String, WidgetBuilder> {
        '/home': (BuildContext context) => HomePage(),
        '/user': (BuildContext context) => UserPage(),
      },
    );
  }
}

