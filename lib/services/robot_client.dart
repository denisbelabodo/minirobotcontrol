import 'dart:math';

import 'package:grpc/grpc.dart';
import 'package:mini_robot_control/mocks/data_mock.dart';
import 'package:mini_robot_control/models/robot_details.dart';
import 'package:mini_robot_control/proto/generated/robot.pbgrpc.dart';

import '../models/harvest_details.dart';

/**
 * Dummy API client
 */
class RobotClient {

  /**
   * Robot list getter
   */
  Future<List<RobotDetails>> getRobotList() async {
    return DataMock.robots.map((r) => RobotDetails(
        r.id,
        (r.activated != 0)? true:false
    )).toList();
  }

  /**
   * Harvest list getter
   */
  Future<HarvestData> getHarvestList() async {
    return HarvestData(DataMock.harvestMaxWeight, DataMock.harvestMinWeight, DataMock.harvests.map((h) => HarvestDetails(
        h.robot,
        DateTime.parse(h.date),
        h.weight,
        Duration.zero)
    ).toList());
  }

  /**
   * save new harvest
   */
  Future<void> saveHarvest(Harvest harvest) async {
    DataMock.harvests.add(harvest);
  }
}