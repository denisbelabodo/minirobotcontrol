import 'package:flutter/material.dart';
import 'package:mini_robot_control/layout/app_layout.dart';
import 'package:mini_robot_control/layout/responsive.dart';
import 'package:mini_robot_control/sections/harvest_data.dart';
import 'package:mini_robot_control/sections/robot_list.dart';
import 'package:mini_robot_control/styles/styles.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: AppLayout(
          activeTab: 0,
          content: Row(
            children: [
              const Expanded(
                  child: HarvestData()
              ),
              Visibility(
                visible: Responsive.isDesktop(context),
                child: Padding(
                  padding: EdgeInsets.only(left: Styles.defaultPadding),
                  child: const RobotList(),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
