import 'package:flutter/material.dart';
import 'package:mini_robot_control/layout/responsive.dart';
import 'package:mini_robot_control/services/user_service.dart';

import '../layout/app_layout.dart';
import '../styles/styles.dart';

class UserPage extends StatefulWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  final userService = UserService();

  @override
  Widget build(BuildContext context) {
    final currentUser = userService.getCurrentUser();
    return Scaffold(
        body: SafeArea(
          child: AppLayout(
            activeTab: 1,
            content: Responsive(
                mobile: Container(
                        constraints: const BoxConstraints(minWidth: 300),
                        decoration: BoxDecoration(
                            borderRadius: Styles.defaultBorderRadius, color: Colors.white),
                        padding: EdgeInsets.all(Styles.defaultPadding),
                        margin: EdgeInsets.all(Styles.defaultMargin),
                        child: userForm(currentUser)
                ),
                desktop: Row(
                  children: [
                    Container(
                        constraints: const BoxConstraints(minWidth: 600),
                        decoration: BoxDecoration(
                            borderRadius: Styles.defaultBorderRadius, color: Colors.white),
                        padding: EdgeInsets.all(Styles.defaultPadding),
                        margin: EdgeInsets.all(Styles.defaultMargin),
                        child:userForm(currentUser)
                    )
                  ],
                )
            )
          )
        )
    );
  }

  Widget userForm(User user) {
    return Row(
      children: [
        Padding (
          padding: EdgeInsets.all(Styles.defaultPadding),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text("Username:"),
              Text("Role:"),
            ],
          )
        ),
        Padding(
          padding: EdgeInsets.all(Styles.defaultPadding),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(user.name),
              Text(user.role),
            ],
          )
        )
      ],
    );
  }
}
