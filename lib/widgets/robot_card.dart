import 'package:flutter/material.dart';
import 'package:mini_robot_control/models/robot_details.dart';

import '../styles/styles.dart';


/**
 * View for RobotDetails
 */
class RobotCard extends StatelessWidget {
  final RobotDetails details;
  const RobotCard({Key? key, required this.details}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(18),
      decoration: BoxDecoration(
        color: Styles.defaultLightWhiteColor,
        borderRadius: Styles.defaultBorderRadius,
      ),
      margin: const EdgeInsets.all(10),
      child: Column(
        children: [
          Text(
              details.id,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          Icon(Icons.bolt, color: (details.activated)?Styles.defaultYellowColor:Styles.defaultGreyColor),
        ],
      ),
    );
  }
}
