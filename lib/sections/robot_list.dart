import 'package:flutter/material.dart';
import 'package:mini_robot_control/models/robot_details.dart';
import 'package:mini_robot_control/widgets/robot_card.dart';

import '../services/robot_client.dart';
import '../widgets/category_box.dart';

class RobotList extends StatefulWidget {
  const RobotList({Key? key}) : super(key: key);

  @override
  _RobotListState createState() => _RobotListState();
}

class _RobotListState extends State<RobotList> {
  final data = RobotClient();
  final List<RobotDetails> robots = <RobotDetails>[];

  var loadError = false;
  var lastError = Error();

  @override
  Widget build(BuildContext context) {
    data.getRobotList().then((value) => { if (mounted) setState(() {
      robots.clear();
      robots.addAll(value);
    })});

    return CategoryBox(
      title: "Robots",
      suffix: Container(),
      children: robots
          .map((RobotDetails details) => RobotCard(details: details))
          .toList(),
    );
  }
}
