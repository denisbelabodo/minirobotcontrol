import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:mini_robot_control/models/harvest_details.dart';
import 'package:mini_robot_control/services/robot_client.dart';
import 'package:mini_robot_control/styles/styles.dart';

import '../proto/generated/robot.pb.dart';

/**
 * Main section of HomePage
 * Display informations about Harvests
 */
class HarvestData extends StatefulWidget {
  const HarvestData({Key? key}) : super(key: key);

  @override
  _HarvestDataState createState() => _HarvestDataState();
}

class _HarvestDataState extends State<HarvestData> {
  final data = RobotClient();


  var harvestList = <HarvestDetails>[];
  var harvestChart = <String, List<FlSpot>>{};

  /**
   * Data table config
   */
  final columns = ["Date", "Kg", "Robot"];
  int sortColumn = 0;
  bool isAscending = false;
  double count = 1.0;
  bool callFailed = false;
  double min = 0.0;
  double max = 5000.0;

  /**
   * Chart config
   */
  int minDay = 1;
  int maxDay = 7;

  /**
   * Form data
   */
  String date = "";
  String weight = "";
  String robot = "";

  /**
   * Data for chart lines
   */
  List<LineChartBarData> get robotCharBarData => harvestChart.entries.map((e) =>  LineChartBarData(
    isCurved: true,
    colors: [Colors.blue],
    barWidth: 1,
    isStrokeCapRound: true,
    dotData: FlDotData(show: false),
    belowBarData: BarAreaData(show: false),
    spots: e.value,
  )).toList();

  @override
  Widget build(BuildContext context) {

    // Call API
    data.getHarvestList()
        .then((value) => setState(() {
          // Get boundaries for chart
          max = value.max;
          min = value.min;

          // Prepare chart data
          harvestChart.clear();
          final harvests = value.harvests;
          harvests.sort((a, b) => a.date.compareTo(b.date));

          minDay = value.harvests.first.date.day;
          maxDay = value.harvests.last.date.day;

          for (final harvest in harvests) {
            if (!harvestChart.containsKey(harvest.id)) {
              harvestChart[harvest.id] = [FlSpot(
                  harvest.date.day.toDouble(),
                  harvest.weight
              )];
            } else {
              harvestChart[harvest.id]?.add(FlSpot(
                  harvest.date.day.toDouble(),
                  harvest.weight
              ));
            }
          }

          // Prepare table data
          harvestList = value.harvests;
          harvestList.sort((h1, h2) {
            switch (sortColumn) {
              case 1:
                return isAscending
                    ? h1.weight.compareTo(h2.weight)
                    : h2.weight.compareTo(h1.weight);
              case 2:
                return isAscending
                    ? h1.delay.compareTo(h2.delay)
                    : h2.delay.compareTo(h1.delay);
              case 3:
                return isAscending ? h1.id.compareTo(h2.id) : h2.id.compareTo(h1.id);
              default:
                return isAscending
                    ? h1.date.compareTo(h2.date)
                    : h2.date.compareTo(h1.date);
            }
          });

    }))
    .catchError((onError) => callFailed = true);

    return Column(
      children: [
        Expanded(
          flex: 2,
            child: (harvestList.isNotEmpty)? harvestGraph() : const CircularProgressIndicator(),
        ),
        Expanded(
          flex: 3,
            child: (harvestList.isNotEmpty)? harvestDataTable(harvestList) : const CircularProgressIndicator()
        ),
        ElevatedButton(
          onPressed: () {
            Navigator.of(context).push(harvestForm());
          },
          child: const Text('Ajouter')
        )

      ],
    );
  }

  /**
   * Formulaire d'ajout de récolte
   */
  MaterialPageRoute harvestForm() {
    final GlobalKey<FormState> _key = GlobalKey<FormState>();
    return MaterialPageRoute(builder: (context) {
      return SafeArea(child: Scaffold(
        appBar: AppBar(
          title: const Text("Ajouter une récolte"),
        ),
        backgroundColor: Colors.white,
        body: Form(
          key: _key,
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.all(5),
                margin: const EdgeInsets.all(5),
                child: TextFormField(
                  validator: (value) => (value == null || value.isEmpty || DateTime.tryParse(value) == null)? 'Date invalid' : null,
                  decoration: const InputDecoration(
                      labelText: "Date"
                  ),
                  onChanged: (val) {
                    setState(() {
                      date = val;
                    });
                  },
                )
              ),
              Container(
                  padding: const EdgeInsets.all(5),
                  margin: const EdgeInsets.all(5),
                  child: TextFormField(
                    validator: (value) => (value == null || value.isEmpty || double.tryParse(value) == null)? 'Poids invalid' : null,
                    decoration: const InputDecoration(
                      labelText: "Poids"
                    ),
                    onChanged: (val) {
                      setState(() {
                        weight = val;
                      });
                    },
                  )
              ),
              Container(
                  padding: const EdgeInsets.all(5),
                  margin: const EdgeInsets.all(5),
                  child: TextFormField(
                    validator: (value) => (value == null || value.isEmpty)? 'Id du robot requis' : null,
                    decoration: const InputDecoration(
                        labelText: "Robot"
                    ),
                    onChanged: (val) {
                      setState(() {
                        robot = val;
                      });
                    },
                  )
              ),
              ElevatedButton(
                  onPressed: () {
                      if (_key.currentState!.validate()) {
                        data.saveHarvest(Harvest(
                          robot: robot,
                          date: DateTime.parse(date).toString(),
                          weight: double.parse(weight)
                        ));                        
                        Navigator.of(context).pop();
                      }
                    },
                  child: const Text('Valider'))
            ],
          )
        ),
      ));
    });
  }

  Widget harvestGraph() => Container(
      decoration: BoxDecoration(
          borderRadius: Styles.defaultBorderRadius, color: Colors.white),
      padding: EdgeInsets.only(left: Styles.defaultPadding,top: Styles.defaultPadding*2,right: Styles.defaultPadding*2, bottom: Styles.defaultPadding),
      margin: EdgeInsets.all(Styles.defaultMargin),
      child: LineChart(
          LineChartData(
            titlesData: FlTitlesData(
              rightTitles: SideTitles(showTitles: false),
              topTitles: SideTitles(showTitles: false),
              bottomTitles: SideTitles(
                showTitles: true,
                reservedSize: 22,
                interval: 1,
                getTextStyles: (context, value) => const TextStyle(
                    color: Color(0xff1a3e7b),
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
                margin: 8,
              ),
              leftTitles: SideTitles(
                showTitles: true,
                interval: 500,
                getTextStyles: (context, value) => const TextStyle(
                  color: Color(0xff1a3e7b),
                  fontWeight: FontWeight.bold,
                  fontSize: 15,
                ),
                reservedSize: 32,
                margin: 12,
              ),
            ),
            gridData: FlGridData(
              show: false
            ),
            borderData: FlBorderData(
              border: const Border(
                  bottom: BorderSide(color: Color(0xff1a3e7b)),
                  left: BorderSide(color: Color(0xff1a3e7b))
              )
            ),
            lineBarsData: robotCharBarData,
            minX: minDay.toDouble(),
            maxX: maxDay.toDouble(),
            maxY: max,
            minY: min,
          ))
  );

  Widget harvestDataTable(List<HarvestDetails> harvestList) => Container(
      decoration: BoxDecoration(
          borderRadius: Styles.defaultBorderRadius, color: Colors.white),
      padding: EdgeInsets.all(Styles.defaultPadding),
      margin: EdgeInsets.all(Styles.defaultMargin),
      child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: DataTable(
            sortAscending: isAscending,
            sortColumnIndex: sortColumn,
            columns: getColumns(columns),
            rows: getRows(harvestList),
          )
      )

      );



  List<DataColumn> getColumns(List<String> columns) => columns
      .map((String column) => DataColumn(label: Text(column), onSort: onSort))
      .toList();

  List<DataRow> getRows(List<HarvestDetails> harvests) =>
      harvests.map((HarvestDetails harvest) {
        final cells = [harvest.date, harvest.weight, harvest.id];
        return DataRow(cells: getCells(cells));
      }).toList();

  List<DataCell> getCells(List<dynamic> cells) =>
      cells.map((data) => DataCell(Text('$data'))).toList();

  void onSort(int columnIndex, bool ascending) {
    setState(() {
      sortColumn = columnIndex;
      isAscending = ascending;
    });
  }
}
